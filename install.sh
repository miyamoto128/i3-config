#! /bin/sh

HOME_BIN_DIR=$HOME/bin
HOME_CONF_DIR=$HOME/.config
I3_CONF_DIR=$HOME_CONF_DIR/i3
I3STATUS_CONF_DIR=$HOME_CONF_DIR/i3status


sudo apt update
sudo apt install i3lock i3status libx11-dev make g++ feh scrot pasystray

CURRENT_DIR=$(pwd)
TMP_DIR=$(mktemp -d)
cd $TMP_DIR

# install i3lock-fancy-rapid
if [ ! -e $HOME_BIN_DIR/i3lock-fancy-rapid ]; then
	echo
	echo "Install i3lock-fancy-rapid ..."

	git clone https://github.com/yvbbrjdr/i3lock-fancy-rapid
	cd i3lock-fancy-rapid
	make
	mkdir -p $HOME_BIN_DIR
	cp -u i3lock-fancy-rapid $HOME_BIN_DIR
	cd ..

	echo "..done"
fi

# install i3-battery-popup
if [ ! -e $HOME_BIN_DIR/i3-battery-popup ]; then
	echo
	echo "Install i3-battery-popup ..."

	git clone https://github.com/rjekker/i3-battery-popup.git
	cp -u i3-battery-popup/i3-battery-popup $HOME_BIN_DIR
	cp -u i3-battery-popup/i3-battery-popup.wav $HOME_BIN_DIR

	echo "..done"
fi

rm -rf $TMP_DIR
cd $CURRENT_DIR

# setup new i3 config
echo
echo "Setup i3 ..."

if [ -e $I3_CONF_DIR ]; then
	BACKUP=${I3_CONF_DIR}.bak.$(date +%s)
	echo "A configuration already exists, backup as $BACKUP"
	mv $I3_CONF_DIR $BACKUP
fi
cp -R i3 $HOME_CONF_DIR

echo "..done"

# setup new i3status config
echo
echo "Setup i3status ..."
if [ -e $I3STATUS_CONF_DIR ]; then
	BACKUP=${I3STATUS_CONF_DIR}.bak.$(date +%s)
	echo "A configuration already exists, backup as $BACKUP"
	mv $I3STATUS_CONF_DIR $BACKUP
fi
cp -R i3status $HOME_CONF_DIR

echo "..done"


mkdir -p $HOME/Images/wallpapers
mkdir -p $HOME/Images/Screenshots

echo
echo "Ensure $HOME_BIN_DIR is in your PATH."
echo
echo "Refresh your i3 using Shift + Meta + r to apply the new configuration."
echo
